import pulsectl


class PulseAudio():

	def __init__(self, idle_add, handler, sink_names):
		self.handler = handler
		self.sink_names = sink_names
		idle_add(self.handle)


	def get_sink_volume(self, pulse, name):
		try:
			sinks = [ s for s in pulse.sink_input_list() if s.name == name ]
			if sinks:
				first = next(iter(sinks))
				return str(int(first.volume.value_flat * 100))
			return 0
		except Exception as e:
			print(e)

	def get_sink_volumes(self, sink_names):
  		with pulsectl.Pulse('polystatus') as pulse:
   			return { s: self.get_sink_volume(pulse, s) for s in sink_names }

	def handle(self):
		pulse_sink_vols = self.get_sink_volumes(self.sink_names)
		self.handler.load()
		self.handler.status['pulseaudio'] = pulse_sink_vols
		self.handler.write()
		return True

