#!/usr/bin/env python3

import dbus

class Spotify():
	
	OBJECT =  ('org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2')
	INTERFACE = 'org.freedesktop.DBus.Properties'
	STATUS_DISPLAY = {
		"Playing": "", "Paused": ""
	}

	def __init__(self, bus, handler, output_format='{artist}: {song}', truncate_length=25):
		self.bus = bus
		self.output_format = output_format
		self.truncate_length = truncate_length
		self.handler = handler

		try:
			self.obj = self.bus.get_object(*self.OBJECT)
			self.interface = dbus.Interface(self.obj, self.INTERFACE)
		except Exception as e:
			self.err(e)
			raise e
		self.interface.connect_to_signal('PropertiesChanged', self.on_signal, path_keyword='path')

	def write(self, status, title):
		self.handler.load()
		self.handler.status['spotify'] = { 'title': title, 'status': status, 'display': self.STATUS_DISPLAY.get(status) }
		self.handler.status['spotify']['status'] = status
		self.handler.status['spotify']['title']  = title
		self.handler.write()

	def on_signal(self, sender, value, event, **kwargs):
		metadata = value.get('Metadata')
		artist = metadata['xesam:artist'][0]
		song = self._truncandparen(metadata['xesam:title'], self.truncate_length)
		title = self.output_format.format(artist=artist, song=song)

		playback = value.get('PlaybackStatus')
		self.write(playback, title)

	def _truncandparen(self, song, trunclen):
		if len(song) > trunclen:
			song = song[0:trunclen]
			song += '...' 
		
			if ('(' in song) and (')' not in song):
				song += ')'	
		return song

	def get_song_info(self):
		return self.value

	def err(self, e):
		if isinstance(e, dbus.exceptions.DBusException):
			print('Spotify not running')
		else:
			print(e)



