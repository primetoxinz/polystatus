#!/usr/bin/env python3
from setproctitle import setproctitle
setproctitle("polystatus")
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib
from businfo.spotify import Spotify

from pulseaudio.volume import PulseAudio

import dbus
import json

DBusGMainLoop(set_as_default=True)


FILE = '/tmp/polystatus.json'

with open(FILE, 'w') as f:
	f.write('{}')


class Handler():
	status = {}

	def load(self):
		with open(FILE, 'r') as f:
			self.status = json.load(f)

	def write(self):
		with open(FILE, 'w') as f:
			json.dump(self.status, f)
			f.truncate()

handler = Handler()

bus = dbus.SessionBus()

Spotify(bus, handler)
PulseAudio(GLib.idle_add, handler, [ 'Spotify', 'playStream' ])

try:
	GLib.MainLoop().run()
except KeyboardInterrupt:
	GLib.MainLoop().quit()
